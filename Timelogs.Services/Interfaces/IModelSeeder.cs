﻿using Timelogs.Database;

namespace Timelogs.Services.Interfaces
{
    public interface IModelSeeder
    {
        Task SeedAsync(AppDbContext _dbContext);
    }
}
