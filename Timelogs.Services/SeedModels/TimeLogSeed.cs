﻿using Microsoft.EntityFrameworkCore;
using Timelogs.Models.Models;
using Timelogs.Services.Interfaces;

namespace Timelogs.Database.SeedModels
{
    public class TimeLogSeed : IModelSeeder
    {
        public async Task SeedAsync(AppDbContext _dbContext)
        {
            if (await _dbContext.TimeLogs.AnyAsync())
            {
                return;
            }

            var random = new Random();

            var userIds = await _dbContext.Users.Select(u => u.Id).ToListAsync();
            var projectIds = await _dbContext.Projects.Select(p => p.Id).ToListAsync();

            var timeLogs = new List<TimeLog>();

            for (int i = 0; i < random.Next(1, 21); i++)
            {
                var timeLog = new TimeLog
                {
                    UserId = userIds[random.Next(userIds.Count)],
                    ProjectId = projectIds[random.Next(projectIds.Count)],
                    Date = DateTime.Now.Subtract(TimeSpan.FromDays(random.Next(365 * 40))),
                    Hours = (float)random.NextDouble() * (480 - 25) + 25
                };

                timeLogs.Add(timeLog);
            }

            await _dbContext.TimeLogs.AddRangeAsync(timeLogs);
        }
    }
}
