﻿using Microsoft.EntityFrameworkCore;
using Timelogs.Models.Models;
using Timelogs.Services.Interfaces;

namespace Timelogs.Database.SeedModels
{
    public class ProjectSeed : IModelSeeder
    {
        public async Task SeedAsync(AppDbContext _dbContext)
        {
            if (await _dbContext.Projects.AnyAsync())
            {
                return;
            }

            var projects = new List<Project>
        {
            new() { Name = "My own" },
            new() { Name = "Free Time" },
            new() { Name = "Work" }
        };
            await _dbContext.AddRangeAsync(projects);
        }
    }
}
