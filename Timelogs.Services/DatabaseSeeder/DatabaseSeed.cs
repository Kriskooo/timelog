﻿using Microsoft.EntityFrameworkCore;
using System.Transactions;
using Timelogs.Database;
using Timelogs.Database.SeedModels;
using Timelogs.Services.Interfaces;

namespace Timelogs.Services.DatabaseSeeder
{
    public class DatabaseSeed : IDatabaseSeed
    {
        private readonly AppDbContext _dbContext;

        public DatabaseSeed(AppDbContext _dbContext)
        {
            this._dbContext = _dbContext;
        }

        public async Task InitializeDatabase()
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            DisableConstraints();

            await ClearTablesAsync();

            await _dbContext.SaveChangesAsync();

            await SeedDatabase();

            scope.Complete();
        }
        private void DisableConstraints()
        {
            var dbSetProperties = _dbContext.GetType().GetProperties()
                .Where(p => p.PropertyType.IsGenericType
                            && p.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>));

            foreach (var prop in dbSetProperties)
            {
                _dbContext.Database.ExecuteSqlRaw("EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'");
            }
        }

        private async Task ClearTablesAsync()
        {
            var dbSetProperties = _dbContext.GetType().GetProperties()
                .Where(p => p.PropertyType.IsGenericType
                            && p.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>));

            foreach (var prop in dbSetProperties)
            {
                var entityType = prop.PropertyType.GetGenericArguments().First();
                var tableName = _dbContext.Model.FindEntityType(entityType)!.GetTableName();
                var sqlCommand = $"DELETE FROM {tableName}";

                await _dbContext.Database.ExecuteSqlRawAsync(sqlCommand);
            }
        }

        private async Task SeedDatabase()
        {
            var seeders = new List<IModelSeeder>
            {
              new UserSeed(),
              new ProjectSeed(),
              new UserSeed()
            };

            foreach (var seeder in seeders)
            {
                await seeder.SeedAsync(_dbContext);
            }
            await _dbContext.SaveChangesAsync();
        }
    }
}
