﻿using System.ComponentModel.DataAnnotations;
using Timelogs.Models.Models.Abstraction;

namespace Timelogs.Models.Models
{
    public class Project : AbstractModel
    {
        public Project()
        {
            this.TimeLogs = new List<TimeLog>();
        }

        [Required]
        public string Name { get; set; }
        public ICollection<TimeLog> TimeLogs { get; set; }
    }
}
