﻿using System.ComponentModel.DataAnnotations;

namespace Timelogs.Models.Models.Abstraction
{
    public class AbstractModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime DeletedOn { get; }
        public bool IsDeleted { get; set; }
    }
}
