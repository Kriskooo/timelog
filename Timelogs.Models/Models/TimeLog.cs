﻿using Timelogs.Models.Models.Abstraction;

namespace Timelogs.Models.Models
{
    public class TimeLog : AbstractModel
    {
        public int? UserId { get; set; }
        public User? User { get; set; }
        public float Hours { get; set; }
        public int? ProjectId { get; set; }
        public Project? ProjectDetails { get; set; }
        public DateTime Date { get; set; }
    }
}
